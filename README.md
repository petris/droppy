# Droppy

Droppy is a simple application that allows transmission of files securly in realtime. No transferred content is stored in the process of facilitating a transfer from the sender to the recipient, not can the server tell what is in the transferred file as the file is encrypted and decrypted on the web client. This is similar to how [0bin](https://github.com/sametmax/0bin) works, however nothing is stored.


## Project Location

A public instance of this project is hosted on [Heroku](https://www.heroku.com/) at [droppy.us](https://droppy.us/), and the source code is primary hosted on [GitLab](https://gitlab.com/petris/droppy). If you're browsing this source on GitHub or any other location, please visit [gitlab.com/petris/droppy](https://gitlab.com/petris/droppy) to get the latest copy.

## License

Droppy is licensed under the GNU Affero General Public License Version 3. A copy of this license is available in the LICENSE file.