'use strict';

var express = require('express');
var app = express();
var http = require('http').Server(app);

app.use(express.static(__dirname + '/web'));

app.get('/r/*', function(req, res, next) {
    res.sendFile(__dirname + '/web/index.html');
});

require('./socket.js').load(http);

http.listen(process.env.PORT || 3000);