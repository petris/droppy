'use strict';

(function(angular) {
    var app = angular.module('droppy');
    
    var controller = function($scope, $location, socket, upload, download, crypto) {
        $scope.files = [];
        $scope.room = {};
        $scope.host = $location.host();
        
        $scope.key = $location.hash();
        download.setKey($scope.key);
        
        var getRoomId = function() {
            var parts = $location.path().split('/');

            if (parts.length >= 3) {
                return parts[2];
            }
            
            return null;
        };
        
        var regenerateKey = function() {
            $scope.key = crypto.random(32);
            $location.hash($scope.key);
            download.setKey($scope.key);
        };
        
        $scope.uploadChange = function(file) {
            $scope.files.push(upload.upload(file, $scope.key));
        };
        
        download.addListener(function(file) {
            $scope.files.push(file);
        });
        
        var currentRoomId = getRoomId();
        
        socket.on('join', function(data) {
            $scope.room.id = data.roomId;

            if ($scope.room.id != currentRoomId) {
                $location.path('/r/' + $scope.room.id);
                regenerateKey();
            }
            
            currentRoomId = $scope.room.id;
        });
        
        socket.on('room.info', function(data) {
            $scope.room.count = data.count;
        });
        
        var onReconnect = function() {
            socket.emit('join', {
                roomId: currentRoomId
            });
        };
        
        socket.on('reconnect', onReconnect);
        
        onReconnect();
    };
    
    controller.$inject = ['$scope', '$location', 'droppySocket', 'droppyUpload', 'droppyDownload', 'droppyCrypto'];
    
    app.controller('droppyController', controller);
})(angular);