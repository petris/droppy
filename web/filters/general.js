'use strict';

(function(angular) {
    var app = angular.module('droppy');

    var filter = function() {
        return function(items) {
            return items.slice().reverse();
        };
    };
    
    filter.$inject = [];

    app.filter('reverse', filter);
})(angular);

(function(angular) {
    var app = angular.module('droppy');

    var filter = function() {
        return function(val) {
            return Math.round(val * 100) / 100;
        };
    };
    
    filter.$inject = [];

    app.filter('round', filter);
})(angular);