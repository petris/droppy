'use strict';

(function(angular) {
    var app = angular.module('droppy');
    
    var config = function($locationProvider, $compileProvider) {
        $locationProvider.html5Mode(true).hashPrefix('!');
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):/);
    }
    
    config.$inject = ['$locationProvider', '$compileProvider'];
    
    app.config(config);
})(angular);