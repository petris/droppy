'use strict';

(function(angular) {
    var app = angular.module('droppy');
    
    var directive = function($rootScope) {
        var link = function(scope, element, attrs, controller) {
            var body = element.parent('body');
            
            var processDrag = function(e) {
                e.preventDefault();
                e.originalEvent.dataTransfer.effectAllowed = 'copy';
                
                return false;
            };
            
            body.bind('dragover', processDrag);
            body.bind('dragenter', processDrag);

            body.bind('drop', function(e) {
                e.preventDefault();
                
                var files = e.originalEvent.dataTransfer.files;
                
                for (var i = 0; i < files.length; i++) {
                    scope.change(files[i]);
                }
                
                return false;
            });
        };
        
        return {
            restrict: 'A',
            scope: {
                change: "=dChange"
            },
            link: link
        }
    };
    
    directive.$inject = ['$rootScope'];
    
    app.directive('droppyUpload', directive);
})(angular);