'use strict';

(function(angular) {
    var app = angular.module('droppy');
    
    var directive = function() {
        var link = function(scope, element, attrs, controller) {
            scope.download = function() {
                saveAs(scope.file.blob, scope.file.name);
            };
        };
        
        return {
            restrict: 'A',
            templateUrl: '/directives/droppyFile.html',
            scope: {
                file: '=dFile'
            },
            link: link
        };
    };
    
    directive.$inject = [];
    
    app.directive('droppyFile', directive);
})(angular);