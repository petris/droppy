'use strict';

(function(angular) {
    var app = angular.module('droppy');

    var factory = function($rootScope, io) {
        var socket = io();
        
        var wrap = function(next) {
            return function() {
                var args = arguments;
                
                $rootScope.$apply(function() {
                    return next.apply(this, args);
                });
            }
        };
        
        return {
            on: function(event, next) {
                socket.on(event, wrap(next));
            },
            emit: function(event, data) {
                socket.emit(event, data);
            }
        };
    };
    
    factory.$inject = ['$rootScope', 'socket.io'];
    
    app.factory('droppySocket', factory);
})(angular);