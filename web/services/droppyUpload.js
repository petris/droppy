'use strict';

(function(angular) {
    var app = angular.module('droppy');
    
    var factory = function($rootScope, socket, crypto) {
        var chunkSize = 128 * 1024; // 128 kb
        var ackCallbacks = {};

        socket.on('file.ack', function(data) {
            if (ackCallbacks[data.guid]) {
                ackCallbacks[data.guid](data);
            }
        });
        
        var upload = function(file, key) {
            var newFile = {
                name: file.name,
                size: file.size,
                type: file.type,
                guid: crypto.random(64),
                progress: 0,
                blob: null
            }
            
            socket.emit('file.start', newFile);
            
            var chunks = [];
            
            // inspired by http://code.tutsplus.com/tutorials/how-to-create-a-resumable-video-uploader-in-nodejs--net-25445
            var reader = new FileReader();
            var slice = file.slice || file.webkitSlice || file.mozSlice;
            
            var currentOffset = 0;
            var currentSize = 0;
            var currentChunk = 0;
            
            var startChunk = function(chunkId) {
                currentOffset = chunkSize * chunkId;
                currentSize = Math.min(chunkSize, newFile.size - currentOffset);
                
                var chunk = slice.call(file, currentOffset, currentOffset + currentSize);
                
                reader.readAsArrayBuffer(chunk);
            };
            
            reader.onload = function(e) {
                var chunk = e.target.result;
                var iv = crypto.random(32);
                
                chunks.push(chunk);
                
                var encryptedChunk = crypto.encrypt(chunk, key, iv);
                
                socket.emit('file.chunk', {
                    guid: newFile.guid,
                    chunk: encryptedChunk,
                    offset: currentOffset,
                    size: currentSize,
                    iv: iv
                });
            };
            
            var ackCallback = function(data) {
                newFile.progress = Math.round(((currentOffset + currentSize) / newFile.size) * 100);

                if ((currentOffset + currentSize) < newFile.size) {
                    startChunk(currentChunk++);
                    return;
                }
                
                newFile.blob = new Blob(chunks, { type: newFile.type });
                
                // cleanup
                chunks = [];
                delete ackCallbacks[newFile.guid];
            };
            
            ackCallbacks[newFile.guid] = ackCallback;
            
            startChunk(currentChunk++);
            
            return newFile;
        };
        
        return {
            upload: upload,
            ackCallbacks: ackCallbacks
        };
    };
    
    factory.$inject = ['$rootScope', 'droppySocket', 'droppyCrypto'];
    
    app.factory('droppyUpload', factory);
})(angular);