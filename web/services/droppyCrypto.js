'use strict';

(function(angular) {
    var app = angular.module('droppy');
    
    var factory = function($rootScope, forge, _, $) {
        var stringToArrayBuffer = function (str) {
            var buf = new ArrayBuffer(str.length);
            var bufView = new Uint8Array(buf);
            
            for (var i = 0; i < str.length; i++) {
                bufView[i] = str.charCodeAt(i);
            }
            
            return buf;
        };
        
        var generateRandomNumber = function(length) {
            var bytes = forge.random.getBytesSync(length / 2);
            
            return $.encodeBase62(bytes);
        };
        
        var encryptDecrypt = function(method, buffer, key, iv) {
            var keyBinary = $.decodeBase62(key);
            var ivBinary = $.decodeBase62(iv);
            
            var cipher = null;
            
            switch (method) {
                case 'encrypt':
                    cipher = forge.cipher.createCipher('AES-CBC', keyBinary);
                    break;
                    
                case 'decrypt':
                    cipher = forge.cipher.createDecipher('AES-CBC', keyBinary);
                    break;
                    
            }
            
            cipher.start({iv: ivBinary});
            cipher.update(forge.util.createBuffer(buffer));
            cipher.finish();
            
            return stringToArrayBuffer(cipher.output.getBytes());
        };
        
        return {
            encrypt: _.partial(encryptDecrypt, 'encrypt'),
            decrypt: _.partial(encryptDecrypt, 'decrypt'),
            random: generateRandomNumber
        };
    };
    
    factory.$inject = ['$rootScope', 'forge', 'underscore', 'jQuery'];
    
    app.factory('droppyCrypto', factory);
})(angular);