'use strict';

(function(angular) {
    var app = angular.module('droppy');
    
    var factory = function(socket, crypto) {
        var files = {};
        var listeners = [];
        var key = null;

        socket.on('file.start', function(data) {
            var file = {
                file: data,
                chunks: []
            };
            
            files[file.file.guid] = file;
            
            for (var i = 0; i < listeners.length; i++) {
                listeners[i](file.file);
            }
        });
        
        socket.on('file.chunk', function(data) {
            if (!files[data.guid]) {
                return;
            }
            
            var file = files[data.guid];
            var decryptedChunk = crypto.decrypt(data.chunk, key, data.iv);
            
            file.chunks.push(decryptedChunk);
            file.file.progress = Math.round(((data.offset + data.size) / file.file.size) * 100);

            if (data.offset + data.size < file.file.size) {
                return;
            }
            
            file.file.blob = new Blob(file.chunks, { type: file.file.type });

            // cleanup
            file.chunks = [];
            delete files[file.file.guid];
        });

        var addListener = function(next) {
            listeners.push(next);
        };
        
        var setKey = function(newKey) {
            key = newKey;
        };
        
        return {
            addListener: addListener,
            setKey: setKey
        }
    };
    
    factory.$inject = ['droppySocket', 'droppyCrypto'];
    
    app.factory('droppyDownload', factory);
})(angular);