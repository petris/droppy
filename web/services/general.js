'use strict';

(function(angular) {
    var app = angular.module('droppy');

    var factory = function() {
        return window.io;
    }
    
    factory.$inject = [];
    
    app.factory('socket.io', factory);
})(angular);

(function(angular) {
    var app = angular.module('droppy');

    var factory = function() {
        return window._;
    }
    
    factory.$inject = [];
    
    app.factory('underscore', factory);
})(angular);

(function(angular) {
    var app = angular.module('droppy');

    var factory = function() {
        return window.forge;
    }
    
    factory.$inject = [];
    
    app.factory('forge', factory);
})(angular);

(function(angular) {
    var app = angular.module('droppy');

    var factory = function() {
        return window.$;
    }
    
    factory.$inject = [];
    
    app.factory('jQuery', factory);
})(angular);